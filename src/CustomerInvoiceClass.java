import javax.sound.sampled.SourceDataLine;

public class CustomerInvoiceClass {
    public static void main(String[] args) {
        Customer customer1 = new Customer(1, "Hải", 50);
        Customer customer2 = new Customer(2, "Hoàng Hải", 30);

        System.out.println( "customer1 : " + customer1.toString() + "\n" +
                            "customer2 : " + customer2.toString());
        Invoice invoice1 = new Invoice(1 , customer1 , 50000);
        Invoice invoice2 = new Invoice(2 , customer2 ,50000);        
        
        System.out.println( " invoice1: " + invoice1.toString() + "\n" +
                            " invoice2: " + invoice2.toString());
    }
    
}
